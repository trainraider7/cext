/* 
Copyright 2021 Robert Rapier

    This file is part of CExt.

    CExt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    CExt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General 
    Public License along with CExt.  If not, see 
    <https://www.gnu.org/licenses/>.

*/

#define DEBUG_ARRAY

#include "../dynamic_array.h"

int main() 
{       
        size_t arrsize = 5;
        int * intarr = arr_alloc(int, arrsize);

        for(int i = 0; i < arr_len(intarr); ++i) {
                intarr[i] = i;
        }
        for(int i = intarr[_ele_last(intarr)] + 1; i < 247; i++){
                arr_append(intarr, i);
        }

        printf( "\n\nArray 1\n"
                "Initialized and filled past initial size.\n" 
                "Expanded to fit.\n\n");
        arr_print_debug(intarr);

        arr_clear(intarr);
        for(int i = 0; i < 8; ++i) {
                arr_append(intarr, i*2);
        }

        int * copyarr = arr_copy(intarr);

        for(int i = _ele_last(copyarr) + 1; i < 11; i++){
                arr_append(copyarr, copyarr[i - 1] * copyarr[i - 1]);
        }

        printf( "\n\nArray 1\n"
                "Cleared, and filled with new values\n\n");
        arr_print_debug(intarr);

        printf( "\n\nArray 2\n"
                "Copied from Array 1 and added new values.\n\n");
        arr_print_debug(copyarr);

        arr_zero( (void *)copyarr);
        printf( "\n\nArray 2\n"
                "Filled with zeros\n\n");
        arr_print_debug(copyarr);
        
        arr_free(intarr);
        printf("\nIntentionally didn't free Array 2\n\n");
        /*arr_free(copyarr); */
        arr_mem_check;
        return 0;
}
