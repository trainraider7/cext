/* 
Copyright 2021 Robert Rapier

    This file is part of CExt.

    CExt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    CExt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General 
    Public License along with CExt.  If not, see 
    <https://www.gnu.org/licenses/>.

*/

/* 
 Written in a non-standard C dialect: GNUC 11+

 CONFIGURATION:

        You may configure this library by defining certain macros before inclusion, or by 
        passing an argument to GCC/Clang of the form:
                -D[MACRO_NAME] [VALUE]
        e.g.
                -DPAGE_MULTIPLIER 64UL

        MACROS:

        DEBUG_ARRAY
                Activates debugging functionality if defined at all.
                Activates arr_mem_check and arr_print_debug which do nothing otherwise.

        PAGE_MULTIPLIER
                Defaults to 32UL. Used to allocated memory which is aligned to a multiple of
                PAGE_MULTIPLIER and the size of a void pointer. Maybe it improves performance.
                Maybe its a placebo. Higher values will raise memory usage, but reduce calls
                for more memory.

*/

#ifndef RESARRAY_H
#define RESARRAY_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../typedefs.h"

#ifndef PAGE_MULTIPLIER
#define PAGE_MULTIPLIER 32UL
#endif  /* PAGE_MULTIPLIER */

const size_t _page_size = PAGE_MULTIPLIER * sizeof(void *); /* Pick a page size which is a power of 2, divisible by sizeof(void *) */
const size_t _slot_size = (sizeof(size_t) > sizeof(void *)) ? sizeof(size_t) : sizeof(void *);
const size_t _arr_reserved = (_slot_size * 5);

#ifdef DEBUG_ARRAY
size_t _active_array_count = 0;
#endif /* DEBUG_ARRAY */


/* Internally used macros for array metadata retrieval ********************************************************************************/
#define _bytes_filled(arr) (*((size_t *)((byte_t *)(arr) - 5 * _slot_size)))        /* Returns bytes used by array elements             */
#define _root_ptr(arr)     ((void *)((byte_t *)(arr) - 5 * _slot_size))     /* Returns low-level pointer used by free(), realloc(), etc */
#define _bytes_alloc(arr)  (*((size_t *)((byte_t *)(arr) - 4 * _slot_size)))        /* Returns bytes allocated by array                 */
#define _ele_last(arr)     (*((size_t *)((byte_t *)(arr) - 3 * _slot_size)) - 1)    /* Returns array index of last element              */
#define _arr_size(arr)     (*((size_t *)((byte_t *)(arr) - 3 * _slot_size)))        /* Returns array size in elements                   */
#define _ele_size(arr)     (*((size_t *)((byte_t *)(arr) - 2 * _slot_size)))        /* Returns the size in bytes of a single element    */
#define _ele_next(arr)     (*(__typeof__(arr) *)((byte_t *)(arr) - 1 * _slot_size)) /* Returns pointer to the first empty spot in array */
/**************************************************************************************************************************************/


/* type* arr_alloc(type, arr_size);
 * 
 *      Creates a dynamically allocated resizeable array of type `type` and size `arr_size`. Array must be freed later with
 *      `arr_free()` to prevent a memory leak.
 *  
 *      PARAMETERS:
 *                      type (type token) : Specify type of array, e.g. `int`, `float`, `void*`. Not a traditional
 *                                          variable parameter, hence this is a macro.
 *                      arr_size (size_t) : Starting size of array measured in how many elements to store, NOT bytes.
 * 
 *      RETURNS:
 *                      type* arrays
 */

#define _arr_alloc(type, arr_size) __extension__ /* Creates an array. Initialize before use to avoid undefined behavior.*/\
({                                                                                                              \
        size_t _size = (arr_size);                                                                              \
        size_t  needed  =       ((_arr_reserved) +                                                              \
                                (sizeof(type) * (_size)) );                                                     \
                                                                                                                \
        size_t allocate = ((needed / _page_size) + 1) * _page_size;                                             \
        byte_t * arr = aligned_alloc(_page_size, allocate);                                                       \
        arr += _arr_reserved;                                                                                   \
        _bytes_filled(arr) = needed;                                                                            \
        _bytes_alloc(arr) = allocate;                                                                           \
        _arr_size(arr) = 0;                                                                                     \
        _arr_size(arr) += _size;                                                                                \
        _ele_size(arr) = sizeof(type);                                                                          \
        _ele_next((type *)arr) = (type *)arr + _size;                                                           \
        RETURN (type *)arr;                                                                                     \
})

#ifdef DEBUG_ARRAY                                                                                      
#define arr_alloc(type, arr_size) __extension__                 \
({                                                              \
        _active_array_count++;                                  \
        RETURN _arr_alloc(type, arr_size);                      \
})
#else
#define arr_alloc(type, arr_size) _arr_alloc(type, arr_size)
#endif /* DEBUG_ARRAY */
/***********************************************************************************************/
/* size_t arr_len(arr);
 * 
 *      Gets the size of array measured as the number of elements (initialized or otherwise), NOT bytes.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 *      RETURNS:
 *                      size_t size : size of array
 */
#define arr_len(arr) (_arr_size(arr))
/***********************************************************************************************/
/* void arr_append(arr, value);
 * 
 *      Expand the dynamic array by one element, and set that element to `value`
 *
 *      PARAMETERS:
 *                      arr (type *) : a dynamic array of generic type.
 *                      value (type) : a value of type to store in the array.
 *
 */

#define arr_append(arr, value)                                                                                          \
do {                                                                                                                    \
        __typeof__(arr) * extern_arr = &(arr);                                                                          \
        __typeof__(arr) _arr = *extern_arr;                                                                             \
        if (_bytes_filled(_arr) + _ele_size(_arr) <= _bytes_alloc(_arr)) {                                              \
                *(_ele_next(_arr)++) = (value);                                                                         \
                _arr_size(_arr)++;                                                                                      \
                _bytes_filled(_arr) += _ele_size(_arr);                                                                 \
        } else {                                                                                                        \
                _arr = (__typeof__(arr))((byte_t *)realloc(_root_ptr(_arr),                                               \
                        _bytes_alloc(_arr) = _bytes_alloc(_arr) << 1) +                                                 \
                        _arr_reserved);                                                                                 \
                *extern_arr = _arr;                                                                                     \
                _ele_next(_arr) = _arr + _arr_size(_arr);                                                           \
                *(_ele_next(_arr)++) = (value);                                                                         \
                _arr_size(_arr)++;                                                                                      \
                _bytes_filled(_arr) += _ele_size(_arr);                                                                 \
        }                                                                                                               \
} NORETURN
/***********************************************************************************************/
/* void arr_free(void * arr);
 * 
 *      Destroys an array and frees its allocated memory.
 *  
 *      PARAMETERS:
 *                      arr (void*) : Pointer to array
 * 
 */
void _arr_free(void * arr) 
{
        free(_root_ptr(arr));
}

#ifdef DEBUG_ARRAY                                                                                      
void arr_free(void * arr)
{
        _active_array_count--;
        _arr_free(arr);
}
#else
void arr_free(void * arr) {_arr_free(arr);}
#endif /* DEBUG_ARRAY */
/***********************************************************************************************/
/* void arr_clear(arr);
 * 
 *      Emptys array. Array pointer may change as memory is reallocated.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 */
#define arr_clear(arr)                          \
do {                                            \
        __typeof__(arr) * _arr = &(arr);        \
        arr_free(*_arr);                        \
        *_arr = arr_alloc(__typeof__(*arr), 0); \
} NORETURN
/***********************************************************************************************/
/* type * arr_copy(arr);
 * 
 *      Creates a new array which is a copy of the input array.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 *      RETURNS:
 *                      type * copy : a copy of the original array.
 */
#define _arr_copy(arr) __extension__                                                            \
({                                                                                              \
        __typeof__(arr) _arr = (arr);                                                           \
        __typeof__(arr) new_arr = aligned_alloc(_page_size,_bytes_alloc(_arr)) + _arr_reserved; \
        memcpy(_root_ptr(new_arr), _root_ptr(_arr), _bytes_filled(_arr));                       \
        _ele_next(new_arr) = new_arr + _arr_size(new_arr);                                  \
        RETURN new_arr;                                                                         \
})

#ifdef DEBUG_ARRAY
#define arr_copy(arr) __extension__                             \
({                                                              \
        _active_array_count++;                                  \
        RETURN _arr_copy(arr);                                  \
})
#else
#define arr_copy(arr) _arr_copy(arr)
#endif /* DEBUG_ARRAY */
/***********************************************************************************************/
/* void arr_zero(arr);
 * 
 *      Fills all bytes of array with 0.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 */

void arr_zero(void * arr)
{
        memset(arr, 0, _bytes_filled(arr) - _arr_reserved);
}
/***********************************************************************************************/
/* void arr_print_debug(arr);
 * 
 *      Prints out array metadata and contents. Only available if macro DEBUG_ARRAY is defined.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 */

#define _arr_print_debug(arr)                           \
do {                                                    \
        __typeof__(arr) _arr = (arr);                   \
        printf( "Array Address:         %p\n"           \
                "Next Empty Address:    %p\n"           \
                "Bytes Used:            %lu\n"          \
                "Bytes Allocated:       %lu\n"          \
                "Last Index:            %li\n"          \
                "Element Size (Bytes):  %lu\n"          \
                "First Element:         %i\n"           \
                "Last Element:          %i\n"           \
                "Array Size:            %lu\n\n\n"      \
                "Array Contents:\n\n",                  \
                (void *)_arr,                           \
                (void *)_ele_next(_arr),                \
                _bytes_filled(_arr),                    \
                _bytes_alloc(_arr),                     \
                _ele_last(_arr),                        \
                _ele_size(_arr),                        \
                _arr[0],                                \
                _arr[_ele_last(_arr)],                  \
                arr_len(_arr)                           \
                );                                      \
        for(int i = 0; i < _ele_last(_arr); ++i){       \
                printf("%i, ", _arr[i]);                \
        }                                               \
        printf("%i\n", _arr[_ele_last(_arr)]);          \
} NORETURN
#ifdef DEBUG_ARRAY
#define arr_print_debug(arr) _arr_print_debug(arr)
#else
#define arr_print_debug(arr)
#endif /* DEBUG_ARRAY */
/***********************************************************************************************/
/* void arr_mem_check(arr);
 * 
 *      Does nothing if arrays haven't leaked memory. If any arrays exist and haven't been freed,
 *      print error message and exit immediately with return code EXIT_FAILURE (1).
 *      Intended to be called in the main function just before return statement.
 *      Only available if macro DEBUG_ARRAY is defined.
 *  
 *      PARAMETERS:
 *                      type * arr : a dynamic array of generic type.
 * 
 */
#ifdef DEBUG_ARRAY                                                                                      
#define arr_mem_check                                                           \
do {                                                                            \
        if (_active_array_count) {                                              \
                printf("ERROR: %li dynamic array(s) not freed!\n", _active_array_count); \
                exit(EXIT_FAILURE);                                             \
        }                                                                       \
} NORETURN
#else
#define arr_mem_check
#endif /* DEBUG_ARRAY */
/***********************************************************************************************/

#endif /* RESARRAY_H */
