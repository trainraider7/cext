/* 
Copyright 2021 Robert Rapier

    This file is part of CExt.

    CExt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as 
    published by the Free Software Foundation, either version 3 of
    the License, or (at your option) any later version.

    CExt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General 
    Public License along with CExt.  If not, see 
    <https://www.gnu.org/licenses/>.

*/
/* Just some basic type definitions */

#ifndef TYPESDEFS_H
#define TYPESDEFS_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/* These are used for cleaner macros */
#define RETURN
#define NORETURN while(0)
/*************************************/

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#endif /* EXIT_SUCCESS */

#ifndef EXIT_FAILURE
#define EXIT_FAILURE 1
#endif /* EXIT_FAILURE */

typedef uint8_t byte_t;

#endif /* TYPESDEFS_H */
